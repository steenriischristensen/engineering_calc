# -*- coding: utf-8 -*-
"""
Created on Sun May 29 21:25:01 2022

@author: u053554
"""

print("Starting to create plots")
plot_filename = 'R'+ path_folder[-6:] +'_U3_Graph.html'
plot_title = 'Plots from U3 Experiment: R'+ path_folder[-6:]
output_file(filename=plot_filename, title=plot_title)
# Figure 1 - Temperature In oven zones
x=data.loc[:,f.U3['runtime']]/3600
y1 = data.loc[:,f.U3['oven_temp_1']]
y2 = data.loc[:,f.U3['oven_temp_2']]
y3 = data.loc[:,f.U3['oven_temp_3']]
y1_l='TIC_331_PV'
y2_l='TIC_332_PV'
y3_l='TIC_333_PV'
p1 = figure(title = "Temperatures in Oven Zones", x_axis_label = 'Time - hrs', y_axis_label='Temperature - C')
p1.line(x,y1, legend_label=y1_l, color = 'blue', line_width=2)
p1.line(x,y2, legend_label=y2_l, color = 'red', line_width=2)
p1.line(x,y3, legend_label=y3_l, color = 'green', line_width=2)
p1.legend.click_policy="mute"
p1.add_tools(HoverTool())
p1.title.text_font_size = '25px'
p1.legend.location = "top_left"
p1.sizing_mode = 'stretch_width'
#show(p1)

# Figure 2 - Temperatures in Filter 
x=data.loc[:,f.U3['runtime']]/3600
y1 = data.loc[:,f.U3['reactor_temp_1']]
y2 = data.loc[:,f.U3['reactor_temp_2']]
y3 = data.loc[:,f.U3['reactor_temp_3']]
y4 = data.loc[:,f.U3['reactor_temp_4']]
y5 = data.loc[:,f.U3['reactor_temp_5']]
y1_l='TI_0304'
y2_l='TI_0305 - Mid point'
y3_l='TI_0306 - Air mid flow'
y4_l='TI_0307 - Before DNO'
y5_l='TI_0308 - After DNO'
p2 = figure(title = "Temperatures inside filter", x_axis_label = 'Time - hrs', y_axis_label='Temperature - C')
p2.line(x,y1, legend_label=y1_l, color = 'blue', line_width=2)
p2.line(x,y2, legend_label=y2_l, color = 'red', line_width=2)
p2.line(x,y3, legend_label=y3_l, color = 'green', line_width=2)
p2.line(x,y4, legend_label=y4_l, color = 'purple', line_width=2)
p2.line(x,y5, legend_label=y5_l, color = 'black', line_width=2)
p2.legend.click_policy="mute"
p2.add_tools(HoverTool())
p2.title.text_font_size = '25px'
p2.legend.location = "top_left"
p2.sizing_mode = 'stretch_width'
#show(p2)

# Figure 3 - FTIR Data 
x=data.loc[:,f.U3['runtime']]/3600
y1 = data.loc[:,f.U3['O2']]
y2 = data.loc[:,f.U3['H2O']]
y3 = data.loc[:,f.U3['NO']]
y4 = data.loc[:,f.U3['NO2']]
y5 = data.loc[:,f.U3['NH3']]
y1_l='O2'
y2_l='H2O'
y3_l='NO'
y4_l='NO2'
y5_l='NH3'
p3 = figure(title = "FTIR Measurements", x_axis_label = 'Time - hrs', y_axis_label='NO, NO2, or NH3 - ppm')
# Setting the second y axis range name and range
p3.extra_y_ranges = {"foo": Range1d(start=0, end=20)}
# Adding the second axis to the plot.  
p3.add_layout(LinearAxis(y_range_name="foo", axis_label='O2 or H2O - %'), 'right')
p3.line(x,y1, legend_label=y1_l, color = 'blue', line_width=2,y_range_name="foo")
p3.line(x,y2, legend_label=y2_l, color = 'yellow', line_width=2,y_range_name="foo")
p3.line(x,y3, legend_label=y3_l, color = 'green', line_width=2)
p3.line(x,y4, legend_label=y4_l, color = 'purple', line_width=2)
p3.line(x,y5, legend_label=y5_l, color = 'red', line_width=2)
p3.legend.click_policy="mute"
p3.add_tools(HoverTool())
p3.title.text_font_size = '25px'
p3.legend.location = "top_left"
p3.sizing_mode = 'stretch_width'
#show(p3)

# Figure 4 - FTIR Data 
x=data.loc[:,f.U3['runtime']]/3600
y1 = data.loc[:,f.U3['CO']]
y2 = data.loc[:,f.U3['CO2']]
y3 = data.loc[:,f.U3['reactor_temp_5']]
y1_l='CO'
y2_l='CO2'
y3_l='Temp. out DNO'

p4 = figure(title = "FTIR Measurements - CO and CO2", x_axis_label = 'Time - hrs', y_axis_label='CO [ppm] or Temp [°C]')
# Setting the second y axis range name and range
p4.extra_y_ranges = {"foo": Range1d(start=0, end=10)}
# Adding the second axis to the plot.  
p4.add_layout(LinearAxis(y_range_name="foo", axis_label='CO2 [%]'), 'right')
p4.line(x,y2, legend_label=y2_l, color = 'yellow', line_width=2,y_range_name="foo")
p4.line(x,y1, legend_label=y1_l, color = 'blue', line_width=2)
p4.line(x,y3, legend_label=y3_l, color = 'green', line_width=2)

p4.legend.click_policy="mute"
p4.add_tools(HoverTool())
p4.title.text_font_size = '25px'
p4.legend.location = "top_left"
p4.sizing_mode = 'stretch_width'
#show(p4)

# Figure 5 - FTIR Data 
x=data.loc[:,f.U3['runtime']]/3600
y1 = data.loc[:,f.U3['Ethanamine']]
y2 = data.loc[:,f.U3['CH4']]
y3 = data.loc[:,f.U3['HCN']]
y4 = data.loc[:,f.U3['CHO']]
y5 = data.loc[:,f.U3['CO']]
y6 = data.loc[:,f.U3['reactor_temp_5']]
y1_l='Ethanamine'
y2_l='Methane- CH4'
y3_l='HCN'
y4_l='Formaldehyde - HCO'
y5_l='CO'
y6_l='Temp. out DNO'
p5 = figure(title = "FTIR Measurements", x_axis_label = 'Time - hrs', y_axis_label='FTIR results [ppm]')
# Setting the second y axis range name and range
p5.extra_y_ranges = {"foo": Range1d(start=0, end=500)}
# Adding the second axis to the plot.  
p5.add_layout(LinearAxis(y_range_name="foo", axis_label='Temp [°C]'), 'right')
p5.line(x,y6, legend_label=y6_l, color = 'black', line_width=2,y_range_name="foo")
p5.line(x,y1, legend_label=y1_l, color = 'blue', line_width=2)
p5.line(x,y2, legend_label=y2_l, color = 'yellow', line_width=2)
p5.line(x,y3, legend_label=y3_l, color = 'green', line_width=2)
p5.line(x,y4, legend_label=y4_l, color = 'purple', line_width=2)
p5.line(x,y5, legend_label=y5_l, color = 'red', line_width=2)


p5.legend.click_policy="mute"
p5.add_tools(HoverTool())
p5.title.text_font_size = '25px'
p5.legend.location = "top_left"
p5.sizing_mode = 'stretch_width'
#show(p5)

# Figure 6 - FTIR Data 
x=data.loc[:,f.U3['runtime']]/3600
y1 = data.loc[:,f.U3['C2H6']]
y2 = data.loc[:,f.U3['C3H8']]
y3 = data.loc[:,f.U3['C3H6']]
y4 = data.loc[:,f.U3['C6H14']]
y5 = data.loc[:,f.U3['C5H12']]
y6 = data.loc[:,f.U3['reactor_temp_5']]
y1_l='Ethan'
y2_l='Propan'
y3_l='Propen'
y4_l='Hexan'
y5_l='Pentan'
y6_l='Temp. out DNO'
p6 = figure(title = "FTIR Measurements", x_axis_label = 'Time - hrs', y_axis_label='FTIR results [ppm]')
# Setting the second y axis range name and range
p6.extra_y_ranges = {"foo": Range1d(start=0, end=500)}
# Adding the second axis to the plot.  
p6.add_layout(LinearAxis(y_range_name="foo", axis_label='Temp [°C]'), 'right')
p6.line(x,y6, legend_label=y6_l, color = 'black', line_width=2,y_range_name="foo")
p6.line(x,y1, legend_label=y1_l, color = 'blue', line_width=2)
p6.line(x,y2, legend_label=y2_l, color = 'yellow', line_width=2)
p6.line(x,y3, legend_label=y3_l, color = 'green', line_width=2)
p6.line(x,y4, legend_label=y4_l, color = 'purple', line_width=2)
p6.line(x,y5, legend_label=y5_l, color = 'red', line_width=2)


p6.legend.click_policy="mute"
p6.add_tools(HoverTool())
p6.title.text_font_size = '25px'
p6.legend.location = "top_left"
p6.sizing_mode = 'stretch_width'
#show(p6)

save(column(children=[p1,p2,p3,p4,p5, p6], sizing_mode='stretch_width'))

# Move the plotted filed
#path_folder = path[0:-21]
original =os.getcwd() + "\\" + plot_filename
target = path_folder + "\\" + plot_filename
shutil.move(original, target)

path_html = path_folder + "\\" + plot_filename
webbrowser.open(path_html)