# -*- coding: utf-8 -*-
"""
Created on Mon Mar 21 21:27:35 2022

@author: u053554, Steen Riis Christensen
"""
import numpy as np
import scipy as sp
from scipy import stats
from nptdms import TdmsFile
from nptdms import tdms
import os
from tkinter import filedialog
import func as f
import inquirer as iq
import pandas as pd
import openpyxl
from openpyxl import load_workbook
import xml.etree.ElementTree as et
import shutil
import webbrowser
from bokeh.plotting import figure, show, output_file, save
from bokeh.models.tools import HoverTool
from bokeh.layouts import row, column
from bokeh.models import LinearAxis, Range1d

# This script is made for data analysis on test units, but made for running...
# in the comand promt
print("\nWelcome to the SECC Test Evaluation Tool\n")


# Ask the user which Unit is being used:
questions_unit = [
  iq.List('Unit',
                message="Choose the Unit that measured the Data:",
                choices=['Unit_1', 'Unit_2', 'Unit_3', 'Monolith_pilot'],
            ),
]
# answers_unit = iq.prompt(questions_unit)

# print(answers_unit["Unit"])

# Promt the User to get the file location
path = filedialog.askopenfilename()

# Assume that an xml file also exist
path_xml = path.replace(".tdms", ".xml")

# Load the data based on User choosen TDMS file
data=f.read_tdms(path)
data = data.iloc[:,0:104]
data.columns=data.columns.str.replace("/'Rekorder1'/","")
data.columns = data.columns.str.replace("'","")
print("\nTDMS Data loaded \n")

# Reading xml data
#xml_data = pd.read_xml(path_xml) 
xml_tree = et.parse(path_xml)
xml_root = xml_tree.getroot()
print("xml data loaded \n")

# Run Number 
xml_run_number = xml_root.findall(path='./test/test_metadata/internal_experiment_id')[0].text.strip()

# U-Number 
xml_unumber = xml_root.findall(path='./test/business_metadata/external_experiment_id')[0].text.strip()

# cat type 
xml_cat = xml_root.findall(path='./test/test_metadata/samples/sample/*/lot')[0].text.strip()

# Mass 
xml_mass = xml_root.findall(path='./test/test_metadata/samples/sample/*/dimension')[0].text.strip()

# Comment 
xml_comment = xml_root.findall(path='./test/test_metadata/samples/sample/*/state')[0].text.strip()

# Creating a datafram containing the xml data:
xmldataf = {'what': ['Run Number', 'U-number', 'Catalyst Type', 'Mass Loaded (g)','Comment'],
        'Values': [xml_run_number, xml_unumber, xml_cat, xml_mass, xml_comment ]}  
  
# Create DataFrame  
xml_df = pd.DataFrame(xmldataf)  
print("xml data saved \n")

print("Copying basic excel sheet\n")

# Get current location
overall_path = os.getcwd()

# Change to correct backslash
overall_path = overall_path.replace(os.sep, '/')

# Direction of the file
overall_path_U3_Filter = overall_path+'/Copy/U3_SCPF.xlsx'

# Destination folder without tdms filename
path_folder = path[0:-21]
path_file = path_folder+"/R"+path_folder[-6:]+".xlsx"

f.copy_file(overall_path_U3_Filter, path_file)
print("Done copying\n")

# Writing data to copyied data file
print("Writing data to raw file\n")

# For testing the datafile can be shortenen
# data=data.iloc[0:1000,0:10]

with pd.ExcelWriter(
        path_file,
        mode="a",
        engine="openpyxl",
        if_sheet_exists="replace",
) as writer:
    data.to_excel(writer, sheet_name="Rekorder1")
    
print("Data written \n")

print("Writting xml data \n")
    
with pd.ExcelWriter(
        path_file,
        mode="a",
        engine="openpyxl",
        if_sheet_exists="replace",
) as writer:
    xml_df.to_excel(writer, sheet_name="Run_info")    

print("xml written\n")

print("Starting to create plots")
plot_filename = 'Experimental_Plots.html'
output_file(filename=plot_filename, title="Plots from experiment")
# Figure 1 - Temperature In oven zones
x=data.loc[:,f.U3['runtime']]/3600
y1 = data.loc[:,f.U3['oven_temp_1']]
y2 = data.loc[:,f.U3['oven_temp_2']]
y3 = data.loc[:,f.U3['oven_temp_3']]
y1_l='TIC_331_PV'
y2_l='TIC_332_PV'
y3_l='TIC_333_PV'
p1 = figure(title = "Temperatures in Oven Zones", x_axis_label = 'Time - hrs', y_axis_label='Temperature - C')
p1.line(x,y1, legend_label=y1_l, color = 'blue', line_width=2)
p1.line(x,y2, legend_label=y2_l, color = 'red', line_width=2)
p1.line(x,y3, legend_label=y3_l, color = 'green', line_width=2)
p1.legend.click_policy="mute"
p1.add_tools(HoverTool())
p1.title.text_font_size = '25px'
p1.legend.location = "top_left"
#show(p1)

# Figure 2 - Temperatures in Filter 
x=data.loc[:,f.U3['runtime']]/3600
y1 = data.loc[:,f.U3['reactor_temp_1']]
y2 = data.loc[:,f.U3['reactor_temp_2']]
y3 = data.loc[:,f.U3['reactor_temp_3']]
y4 = data.loc[:,f.U3['reactor_temp_4']]
y5 = data.loc[:,f.U3['reactor_temp_5']]
y1_l='TI_0304'
y2_l='TI_0305'
y3_l='TI_0306'
y4_l='TI_0307'
y5_l='TI_0308'
p2 = figure(title = "Temperatures inside filter", x_axis_label = 'Time - hrs', y_axis_label='Temperature - C')
p2.line(x,y1, legend_label=y1_l, color = 'blue', line_width=2)
p2.line(x,y2, legend_label=y2_l, color = 'red', line_width=2)
p2.line(x,y3, legend_label=y3_l, color = 'green', line_width=2)
p2.line(x,y4, legend_label=y4_l, color = 'purple', line_width=2)
p2.line(x,y5, legend_label=y5_l, color = 'black', line_width=2)
p2.legend.click_policy="mute"
p2.add_tools(HoverTool())
p2.title.text_font_size = '25px'
p2.legend.location = "top_left"
#show(p2)

# Figure 3 - FTIR Data 
x=data.loc[:,f.U3['runtime']]/3600
y1 = data.loc[:,f.U3['O2']]
y2 = data.loc[:,f.U3['H2O']]
y3 = data.loc[:,f.U3['NO']]
y4 = data.loc[:,f.U3['NO2']]
y5 = data.loc[:,f.U3['NH3']]
y1_l='O2'
y2_l='H2O'
y3_l='NO'
y4_l='NO2'
y5_l='NH3'
p3 = figure(title = "FTIR Measurements", x_axis_label = 'Time - hrs', y_axis_label='NO, NO2, or NH3 - ppm')
# Setting the second y axis range name and range
p3.extra_y_ranges = {"foo": Range1d(start=0, end=20)}
# Adding the second axis to the plot.  
p3.add_layout(LinearAxis(y_range_name="foo", axis_label='O2 or H2O - %'), 'right')
p3.line(x,y1, legend_label=y1_l, color = 'blue', line_width=2,y_range_name="foo")
p3.line(x,y2, legend_label=y2_l, color = 'yellow', line_width=2,y_range_name="foo")
p3.line(x,y3, legend_label=y3_l, color = 'green', line_width=2)
p3.line(x,y4, legend_label=y4_l, color = 'purple', line_width=2)
p3.line(x,y5, legend_label=y5_l, color = 'red', line_width=2)
p3.legend.click_policy="mute"
p3.add_tools(HoverTool())
p3.title.text_font_size = '25px'
p3.legend.location = "top_left"
#show(p3)
save(column(children=[p1,p2,p3], sizing_mode='stretch_width',height=500))

# Move the plotted filed
#path_folder = path[0:-21]
original =os.getcwd() + "\\" + plot_filename
target = path_folder + "\\" + plot_filename
shutil.move(original, target)

path_html = path_folder + "\\" + plot_filename
webbrowser.open(path_html)

print("All DONE - SUCCESS written\n")
