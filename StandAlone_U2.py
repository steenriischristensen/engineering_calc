# -*- coding: utf-8 -*-
"""
Created on Mon Mar 21 21:27:35 2022

@author: u053554, Steen Riis Christensen
"""
import numpy as np
import scipy as sp
from scipy import stats
from nptdms import TdmsFile
from nptdms import tdms
import os
from tkinter import filedialog
import func as f
import inquirer as iq
import pandas as pd
import openpyxl
from openpyxl import load_workbook
import xml.etree.ElementTree as et
import shutil
import webbrowser
from bokeh.plotting import figure, show, output_file, save
from bokeh.models.tools import HoverTool
from bokeh.layouts import row, column
from bokeh.models import LinearAxis, Range1d

# This script is made for data analysis on test units, but made for running...
# in the comand promt
print("\nWelcome to the SECC Test Evaluation Tool\n")

# path='C:/Users/u053554/OneDrive - Umicore/Synced/Documents/Python Scripts/For_Program_test/Unit 2/PST_Unit2_000102/PST_Unit2_000102.tdms'
path='C:/Users/u053554/OneDrive - Umicore/Synced/Documents/Python Scripts/For_Program_test/Unit 2/PST_Unit2_000159/PST_Unit2_000159.tdms'
# Promt the User to get the file location
#path = filedialog.askopenfilename()

# Assume that an xml file also exist
path_xml = path.replace(".tdms", ".xml")

# Load the data based on User choosen TDMS file
data=f.read_tdms(path)
data = data.iloc[:,0:114]
data.columns=data.columns.str.replace("/'Rekorder1'/","")
data.columns = data.columns.str.replace("'","")
print("\nTDMS Data loaded \n")

# Reading xml data
#xml_data = pd.read_xml(path_xml) 
xml_tree = et.parse(path_xml)
xml_root = xml_tree.getroot()
print("xml data loaded \n")

# Run Number 
xml_run_number = xml_root.findall(path='./test/test_metadata/internal_experiment_id')[0].text.strip()

comment=xml_root.findall(path='./test/test_metadata/samples/sample/*/description')[0].text.strip()
for w in f.words(comment,'\n'):
   x=f.words(w,'#')
   for i in range(len(x)):
       if (len(x)>1):
           if ('R5 Run Number' in x[0]):
               r5_run_number = x[1].strip()
           if ('R5 U-numbe' in x[0]):
               r5_unumber = x[1].strip()
           if ('R5 Mass Loaded (g)' in x[0]):
               r5_cat_mass = x[1].strip()
               r5_cat_mass = r5_cat_mass.replace(",", ".")
           if ('R5 Dry Flow Meas (Nl/hr)' in x[0]):
               r5_flow = x[1].strip()
               r5_flow = r5_flow.replace(",", ".")               
           if ('R6 Run Number' in x[0]):
               r6_run_number = x[1].strip()
           if ('R6 U-numbe' in x[0]):
               r6_unumber = x[1].strip()
           if ('R6 Mass Loaded (g)' in x[0]):
               r6_cat_mass = x[1].strip()
               r6_cat_mass = r6_cat_mass.replace(",", ".")
           if ('R6 Dry Flow Meas (Nl/hr)' in x[0]):
               r6_flow = x[1].strip()
               r6_flow = r6_flow.replace(",", ".")                     
           if ('R7 Run Number' in x[0]):
               r7_run_number = x[1].strip()
           if ('R7 U-numbe' in x[0]):
               r7_unumber = x[1].strip()
           if ('R7 Mass Loaded (g)' in x[0]):
               r7_cat_mass = x[1].strip()
               r7_cat_mass = r7_cat_mass.replace(",", ".")
           if ('R7 Dry Flow Meas (Nl/hr)' in x[0]):
               r7_flow = x[1].strip()  
               r7_flow = r7_flow.replace(",", ".")                    
           if ('R8 Run Number' in x[0]):
               r8_run_number = x[1].strip()
           if ('R8 U-numbe' in x[0]):
               r8_unumber = x[1].strip()
           if ('R8 Mass Loaded (g)' in x[0]):
               r8_cat_mass = x[1].strip()
               r8_cat_mass = r8_cat_mass.replace(",", ".")
           if ('R8 Dry Flow Meas (Nl/hr)' in x[0]):
               r8_flow = x[1].strip()
               r8_flow = r8_flow.replace(",", ".")   

# Creating a datafram containing the xml data:
xmldataf = {'what': ['Run Number', 'R5 Run Number', 'R5 U-Number', 'R5 Mass Loaded (g)','R5 Dry flow', 'R6 Run Number', 'R6 U-Number', 'R6 Mass Loaded (g)','R6 Dry flow', 'R7 Run Number', 'R7 U-Number', 'R7 Mass Loaded (g)','R7 Dry flow', 'R8 Run Number', 'R8 U-Number', 'R8 Mass Loaded (g)','R8 Dry flow'],
        'Values': [xml_run_number, r5_run_number, r5_unumber, r5_cat_mass, r5_flow, r6_run_number, r6_unumber, r6_cat_mass, r6_flow, r7_run_number, r7_unumber, r7_cat_mass, r7_flow, r8_run_number, r8_unumber, r8_cat_mass, r8_flow ]}  
  
# Create DataFrame  
xml_df = pd.DataFrame(xmldataf)  
print("xml data saved \n")

print("Copying basic excel sheet\n")

# Get current location
overall_path = os.getcwd()

# Change to correct backslash
overall_path = overall_path.replace(os.sep, '/')

# Direction of the file
overall_path_U3_Filter = overall_path+'/Copy/U2_std_DeNox.xlsx'

# Destination folder without tdms filename
path_folder = path[0:-22]
path_file = path_folder+"/R"+path_folder[-6:]+"_R5-x_R6-x_R7-x_R8-x.xlsx"

f.copy_file(overall_path_U3_Filter, path_file)
print("Done copying\n")

# Writing data to copyied data file
print("Writing data to raw file\n")

# For testing the datafile can be shortenen
# data=data.iloc[0:1000,0:10]

with pd.ExcelWriter(
        path_file,
        mode="a",
        engine="openpyxl",
        if_sheet_exists="replace",
) as writer:
    data.to_excel(writer, sheet_name="Rekorder1")
    
print("Data written \n")

print("Writting xml data \n")
    
with pd.ExcelWriter(
        path_file,
        mode="a",
        engine="openpyxl",
        if_sheet_exists="replace",
) as writer:
    xml_df.to_excel(writer, sheet_name="Run_info")    

print("xml written\n")



print("Starting to create plots")
plot_filename = 'Experimental_Plots.html'
output_file(filename=plot_filename, title="Plots from experiment")
# Figure 1 - Temperature In oven zones
x=data.loc[:,f.U2['runtime']]/3600
y1 = data.loc[:,f.U2['mfc_n2']]
y2 = data.loc[:,f.U2['mfc_no']]
y3 = data.loc[:,f.U2['mfc_so2']]
y4 = data.loc[:,f.U2['mfc_air']]
y1_l='Frontend - N2'
y2_l='Frontend - NO'
y3_l='Frontend - SO2'
y4_l='Frontend - Air'
p1 = figure(title = "Frontend MFC", x_axis_label = 'Time - hrs', y_axis_label='Flow Nl/hr or NmL/hr')
p1.line(x,y1, legend_label=y1_l, color = 'blue', line_width=2)
p1.line(x,y2, legend_label=y2_l, color = 'red', line_width=2)
p1.line(x,y3, legend_label=y3_l, color = 'green', line_width=2)
p1.line(x,y4, legend_label=y4_l, color = 'yellow', line_width=2)
p1.legend.click_policy="mute"
p1.add_tools(HoverTool())
p1.title.text_font_size = '25px'
p1.legend.location = "top_left"
#show(p1)

# Figure 2 - CLD Measurements 
x=data.loc[:,f.U2['runtime']]/3600
y1 = data.loc[:,f.U2['cld_nox']]
y2 = data.loc[:,f.U2['cld_dp']]
y1_l='CLD NOx'
y2_l='CLD DP'
p2 = figure(title = "CLD Measurements", x_axis_label = 'Time - hrs', y_axis_label='NOx [ppm] or DP [mbar]')
p2.line(x,y1, legend_label=y1_l, color = 'blue', line_width=2)
p2.line(x,y2, legend_label=y2_l, color = 'red', line_width=2)
p2.legend.click_policy="mute"
p2.add_tools(HoverTool())
p2.title.text_font_size = '25px'
p2.legend.location = "top_left"
# show(p2)

# Figure 3 - Temperature In R5-R8 Center
x=data.loc[:,f.U2['runtime']]/3600
y1 = data.loc[:,f.U2['r5_temp']]
y2 = data.loc[:,f.U2['r6_temp']]
y3 = data.loc[:,f.U2['r7_temp']]
y4 = data.loc[:,f.U2['r8_temp']]
y1_l='R5 Center'
y2_l='R6 Center'
y3_l='R7 Center'
y4_l='R8 Center'
p3 = figure(title = "Center Temperatures - R5-R8", x_axis_label = 'Time - hrs', y_axis_label='Temperature - °C')
p3.line(x,y1, legend_label=y1_l, color = 'blue', line_width=2)
p3.line(x,y2, legend_label=y2_l, color = 'red', line_width=2)
p3.line(x,y3, legend_label=y3_l, color = 'green', line_width=2)
p3.line(x,y4, legend_label=y4_l, color = 'yellow', line_width=2)
p3.legend.click_policy="mute"
p3.add_tools(HoverTool())
p3.title.text_font_size = '25px'
p3.legend.location = "top_left"
# show(p3)

# Figure 4 - NH3 Flow R5-R8
x=data.loc[:,f.U2['runtime']]/3600
y1 = data.loc[:,f.U2['r5_nh3']]
y2 = data.loc[:,f.U2['r6_nh3']]
y3 = data.loc[:,f.U2['r7_nh3']]
y4 = data.loc[:,f.U2['r8_nh3']]
y1_l='NH3 R5'
y2_l='NH3 R6'
y3_l='NH3 R7'
y4_l='NH3 R8'
p4 = figure(title = "NH3 Flow - R5-R8", x_axis_label = 'Time - hrs', y_axis_label='NH3 Flow - NmL/hr')
p4.line(x,y1, legend_label=y1_l, color = 'blue', line_width=2)
p4.line(x,y2, legend_label=y2_l, color = 'red', line_width=2)
p4.line(x,y3, legend_label=y3_l, color = 'green', line_width=2)
p4.line(x,y4, legend_label=y4_l, color = 'yellow', line_width=2)
p4.legend.click_policy="mute"
p4.add_tools(HoverTool())
p4.title.text_font_size = '25px'
p4.legend.location = "top_left"
# show(p4)

# Figure 5 - Feed, NH3 and H2O flow R5
x=data.loc[:,f.U2['runtime']]/3600
y1 = data.loc[:,f.U2['r5_feed']]
y2 = data.loc[:,f.U2['r5_h2o']]
y3 = data.loc[:,f.U2['r5_nh3']]
y1_l='Feed R5 - [Nl/hr]'
y2_l='H2O R5 - [g/hr]'
y3_l='NH3 R5 - [NmL/hr]'
p5 = figure(title = "R5: Feed, NH3 and H2O flow", x_axis_label = 'Time - hrs', y_axis_label='Flow')
p5.line(x,y1, legend_label=y1_l, color = 'blue', line_width=2)
p5.line(x,y2, legend_label=y2_l, color = 'red', line_width=2)
p5.line(x,y3, legend_label=y3_l, color = 'green', line_width=2)
p5.legend.click_policy="mute"
p5.add_tools(HoverTool())
p5.title.text_font_size = '25px'
p5.legend.location = "top_left"
# show(p5)

# Figure 6 - Feed, NH3 and H2O flow R6
x=data.loc[:,f.U2['runtime']]/3600
y1 = data.loc[:,f.U2['r6_feed']]
y2 = data.loc[:,f.U2['r6_h2o']]
y3 = data.loc[:,f.U2['r6_nh3']]
y1_l='Feed R6 - [Nl/hr]'
y2_l='H2O R6 - [g/hr]'
y3_l='NH3 R6 - [NmL/hr]'
p6 = figure(title = "R6: Feed, NH3 and H2O flow", x_axis_label = 'Time - hrs', y_axis_label='Flow')
p6.line(x,y1, legend_label=y1_l, color = 'blue', line_width=2)
p6.line(x,y2, legend_label=y2_l, color = 'red', line_width=2)
p6.line(x,y3, legend_label=y3_l, color = 'green', line_width=2)
p6.legend.click_policy="mute"
p6.add_tools(HoverTool())
p6.title.text_font_size = '25px'
p6.legend.location = "top_left"
# show(p6)

# Figure 7 - Feed, NH3 and H2O flow R7
x=data.loc[:,f.U2['runtime']]/3600
y1 = data.loc[:,f.U2['r7_feed']]
y2 = data.loc[:,f.U2['r7_h2o']]
y3 = data.loc[:,f.U2['r7_nh3']]
y1_l='Feed R7 - [Nl/hr]'
y2_l='H2O R7 - [g/hr]'
y3_l='NH3 R7 - [NmL/hr]'
p7 = figure(title = "R7: Feed, NH3 and H2O flow", x_axis_label = 'Time - hrs', y_axis_label='Flow')
p7.line(x,y1, legend_label=y1_l, color = 'blue', line_width=2)
p7.line(x,y2, legend_label=y2_l, color = 'red', line_width=2)
p7.line(x,y3, legend_label=y3_l, color = 'green', line_width=2)
p7.legend.click_policy="mute"
p7.add_tools(HoverTool())
p7.title.text_font_size = '25px'
p7.legend.location = "top_left"
# show(p7)

# Figure 8 - Feed, NH3 and H2O flow R8
x=data.loc[:,f.U2['runtime']]/3600
y1 = data.loc[:,f.U2['r8_feed']]
y2 = data.loc[:,f.U2['r8_h2o']]
y3 = data.loc[:,f.U2['r8_nh3']]
y1_l='Feed R8 - [Nl/hr]'
y2_l='H2O R8 - [g/hr]'
y3_l='NH3 R8 - [NmL/hr]'
p8 = figure(title = "R8: Feed, NH3 and H2O flow", x_axis_label = 'Time - hrs', y_axis_label='Flow')
p8.line(x,y1, legend_label=y1_l, color = 'blue', line_width=2)
p8.line(x,y2, legend_label=y2_l, color = 'red', line_width=2)
p8.line(x,y3, legend_label=y3_l, color = 'green', line_width=2)
p8.legend.click_policy="mute"
p8.add_tools(HoverTool())
p8.title.text_font_size = '25px'
p8.legend.location = "top_left"
# show(p8)

save(column(children=[p1,p2,p3,p5,p6,p7,p8], sizing_mode='stretch_width',height=500))

# Move the plotted filed
#path_folder = path[0:-21]
original =os.getcwd() + "\\" + plot_filename
target = path_folder + "\\" + plot_filename
shutil.move(original, target)

path_html = path_folder + "\\" + plot_filename
webbrowser.open(path_html)

print("All DONE - SUCCESS written\n")
