# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import numpy as np
import scipy as sp
from scipy import stats
from nptdms import TdmsFile
from nptdms import tdms
import os
from tkinter import filedialog
import func as f
import xml.etree.ElementTree as et
import pandas as pd

# # --------------------------------------------------------------------------------
# # The following can be used to add text and references to the plot interphase. BUt has not been added yet.
# # https://stackoverflow.com/questions/25877519/how-to-add-text-to-html-files-generated-by-python-bokeh
# from bokeh.io import output_file, show
# from bokeh.layouts import widgetbox
# from bokeh.models.widgets import Div
# from bokeh.models.widgets import Paragraph
# from bokeh.models.widgets import PreText
   
# output_file("div.html")
  
# pre = PreText(text="""Your text is initialized with the 'text' argument. The remaining Paragraph arguments are 'width' and 'height'.""",width=500, height=100)
    
# p = Paragraph(text="""Your text is initialized with the 'text' argument. The remaining Paragraph arguments are 'width' and 'height'""", width=200, height=100)
   
# div = Div(text="""Your <a href="https://en.wikipedia.org/wiki/HTML">HTML</a>-supported text is initialized with the <b>text</b> argument.  The remaining div arguments are <b>width</b> and <b>height</b>. For this example, those values are <i>200</i> and <i>100</i> respectively.""", width=200, height=100)
    
# show(widgetbox(pre, p, div))

# # ------------------------------------------------------------------------------


path='C:/Users/u053554/OneDrive - Umicore/Synced/Documents/Python Scripts/Test folder/PST_SECC_000304.tdms'
# #file_path_string = filedialog.askopenfilename()
# #tdms_file=TdmsFile.read(path)
# #tdms_file1 = TdmsFile(path)

# #tdms_groups = tdms_file.groups()
# #data = tdms_file.as_dataframe()

Data=f.read_tdms(path)
Data_test = Data.columns.str.find("DSC")
Data_end=(Data_test.values != -1).argmax()
Data_ny = Data.iloc[:,0:Data_end]

# path_xml = path.replace(".tdms", ".xml")

# xml_tree = et.parse(path_xml)
# xml_root = xml_tree.getroot()

# # Run Number 
# xml_run_number = xml_root.findall(path='./test/test_metadata/internal_experiment_id')[0].text.strip()

# # U-Number 
# xml_unumber = xml_root.findall(path='./test/business_metadata/external_experiment_id')[0].text.strip()

# # cat type 
# xml_cat = xml_root.findall(path='./test/test_metadata/samples/sample/*/lot')[0].text.strip()

# # Mass 
# xml_mass = xml_root.findall(path='./test/test_metadata/samples/sample/*/dimension')[0].text.strip()

# # Comment 
# xml_comment = xml_root.findall(path='./test/test_metadata/samples/sample/*/state')[0].text.strip()

# # Creating a datafram containing the xml data:
# xmldataf = {'what': ['Run Number', 'U-number', 'Catalyst Type', 'Mass Loaded (g)','Comment'],
#         'Values': [xml_run_number, xml_unumber, xml_cat, xml_mass, xml_comment ]}  
  
# # Create DataFrame  
# xml_df = pd.DataFrame(xmldataf)  

# # #  ----------------------------------------------------------------------


# path='C:/Users/u053554/OneDrive - Umicore/Synced/Documents/Python Scripts/For_Program_test/Unit 2/PST_Unit2_000159/PST_Unit2_000159.tdms'

# path='C:/Users/u053554/OneDrive - Umicore/Synced/Documents/Python Scripts/For_Program_test/Unit 2/PST_Unit2_000159/PST_Unit2_000159.tdms'
# # Promt the User to get the file location
# #path = filedialog.askopenfilename()

# # Assume that an xml file also exist
# path_xml = path.replace(".tdms", ".xml")

# # Load the data based on User choosen TDMS file
# data=f.read_tdms(path)
# data = data.iloc[:,0:114]
# data.columns=data.columns.str.replace("/'Rekorder1'/","")
# data.columns = data.columns.str.replace("'","")
# print("\nTDMS Data loaded \n")

# # Reading xml data
# #xml_data = pd.read_xml(path_xml) 
# xml_tree = et.parse(path_xml)
# xml_root = xml_tree.getroot()
# print("xml data loaded \n")

# # Run Number 
# xml_run_number = xml_root.findall(path='./test/test_metadata/internal_experiment_id')[0].text.strip()

# # U-Number 
# xml_unumber = "TBD"#xml_root.findall(path='./test/business_metadata/external_experiment_id')[0].text.strip()

# # cat type 
# xml_cat = "TBD"#xml_root.findall(path='./test/test_metadata/samples/sample/*/lot')[0].text.strip()

# # Mass 
# xml_mass = "TBD"#xml_root.findall(path='./test/test_metadata/samples/sample/*/dimension')[0].text.strip()

# # Comment 
# xml_comment = "TBD"#xml_root.findall(path='./test/test_metadata/samples/sample/*/state')[0].text.strip()

# comment=xml_root.findall(path='./test/test_metadata/samples/sample/*/description')[0].text.strip()
# for w in f.words(comment,'\n'):
#    x=f.words(w,'#')
#    for i in range(len(x)):
#        if (len(x)>1):
#            if ('R5 Run Number' in x[0]):
#                r5_run_number = x[1].strip()
#            if ('R5 U-numbe' in x[0]):
#                r5_unumber = x[1].strip()
#            if ('R5 Mass Loaded (g)' in x[0]):
#                r5_cat_mass = x[1].strip()
#            if ('R5 Dry Flow Meas (Nl/hr)' in x[0]):
#                r5_flow = x[1].strip()               
#            if ('R6 Run Number' in x[0]):
#                r6_run_number = x[1].strip()
#            if ('R6 U-numbe' in x[0]):
#                r6_unumber = x[1].strip()
#            if ('R6 Mass Loaded (g)' in x[0]):
#                r6_cat_mass = x[1].strip()
#            if ('R6 Dry Flow Meas (Nl/hr)' in x[0]):
#                r6_flow = x[1].strip()                  
#            if ('R7 Run Number' in x[0]):
#                r7_run_number = x[1].strip()
#            if ('R7 U-numbe' in x[0]):
#                r7_unumber = x[1].strip()
#            if ('R7 Mass Loaded (g)' in x[0]):
#                r7_cat_mass = x[1].strip()
#            if ('R7 Dry Flow Meas (Nl/hr)' in x[0]):
#                r7_flow = x[1].strip()                   
#            if ('R8 Run Number' in x[0]):
#                r8_run_number = x[1].strip()
#            if ('R8 U-numbe' in x[0]):
#                r8_unumber = x[1].strip()
#            if ('R8 Mass Loaded (g)' in x[0]):
#                r8_cat_mass = x[1].strip()
#            if ('R8 Dry Flow Meas (Nl/hr)' in x[0]):
#                r8_flow = x[1].strip()                   
               
# # # R5 Run Number #	R5-485
# # # R5 U-number #	STD SG 0
# # # R5 Mass Loaded (g) #	1,9410
# # # R5 Dry Flow Meas (Nl/hr) #	199,044
# # R6	-
# # # R6 Run Number #	R6-440
# # # R6 U-number #	STD SG 1
# # # R6 Mass Loaded (g) #	1.9874
# # # R6 Dry Flow Meas (Nl/hr) #	199,116
# # R7	-
# # # R7 Run Number #	R7-362
# # # R7 U-number #	STD SG 2
# # # R7 Mass Loaded (g) #	1,9914
# # # R7 Dry Flow Meas (Nl/hr) #	197,598
# # R8	-
# # # R8 Run Number #	R8-333
# # # R8 U-number #	STD SG 3
# # # R8 Mass Loaded (g) #	1,9987
# # # R8 Dry Flow Meas (Nl/hr) #	196,932
#            # if ('height' in x[0]):
#            #     metadata ['Height'] = (x[1])
#            # if ('diameter' in x[0]):
#            #     metadata['ReactorDiam'] = (x[1])
#            # if ('SiC' in x[0]):
#            #     metadata['SiC'] = 'Tot. sample + SiC: '+ x[1] +' mg'
#            # if ('sieve' in x[0]):
#            #     metadata['SieveFraction'] =x[1].strip()
#            # if ('name' in x[0]):
#            #     metadata['SampleName']= x[1].strip()
#            # if ('description' in x[0]):
#            #     metadata['SampleDescr']= x[1].strip()
#            # if ('Contact' in x[0]):
#            #     metadata['Contact'] = x[1].strip()
#   #  ----------------------------------------------------------------------
