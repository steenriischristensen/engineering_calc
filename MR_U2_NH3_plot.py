# -*- coding: utf-8 -*-
"""
Created on Thu Jun  9 00:36:16 2022

@author: u053554
"""


print("Starting to create plots")
plot_filename = 'R'+ path_folder[-6:] +'_U2_NH3_Graph.html'
plot_title = 'Plots from U2 Experiment: R'+ path_folder[-6:]
output_file(filename=plot_filename, title=plot_title)
# Figure 1 - Temperature In oven zones
x=data.loc[:,f.U2['runtime']]/3600
y1 = data.loc[:,f.U2['mfc_n2']]
y2 = data.loc[:,f.U2['r5_nh3']]
y3 = data.loc[:,f.U2['mfc_air']]
y4 = data.loc[:,f.U2['r5_h2o']]*22.414/18
y1_l='R1 Dilution N2'
y2_l='R1 NH3 Flow'
y3_l='R2 Airflow'
y4_l='R2 H2O flow'
p1 = figure(title = "Main Flow", x_axis_label = 'Time - hrs', y_axis_label='Flow Nl/hr')
p1.line(x,y1, legend_label=y1_l, color = 'blue', line_width=2)
p1.line(x,y2, legend_label=y2_l, color = 'red', line_width=2)
p1.line(x,y3, legend_label=y3_l, color = 'green', line_width=2)
p1.line(x,y4, legend_label=y4_l, color = 'yellow', line_width=2)
p1.legend.click_policy="mute"
p1.add_tools(HoverTool())
p1.title.text_font_size = '25px'
p1.legend.location = "top_left"
#show(p1)

# Figure 2 - CLD Measurements 
x=data.loc[:,f.U2['runtime']]/3600
y1 = data.loc[:,f.U2['cld_dp']]
y1_l='DP Reactor'
p2 = figure(title = "DP Measurements", x_axis_label = 'Time - hrs', y_axis_label='DP [mbar]')
p2.line(x,y1, legend_label=y1_l, color = 'blue', line_width=2)
p2.legend.click_policy="mute"
p2.add_tools(HoverTool())
p2.title.text_font_size = '25px'
p2.legend.location = "top_left"
# show(p2)

# Figure 3 - Center Temperatures In R5-R8 Center
x=data.loc[:,f.U2['runtime']]/3600
y1 = data.loc[:,f.U2['r5_temp']]
y2 = data.loc[:,f.U2['r6_temp']]
y3 = data.loc[:,f.U2['r7_temp']]
y4 = data.loc[:,f.U2['r8_temp']]
y1_l='R5 Center'
y2_l='R6 Center'
y3_l='R7 Center'
y4_l='R8 Center'
p3 = figure(title = "Center Temperatures", x_axis_label = 'Time - hrs', y_axis_label='Temperature - degC')
p3.line(x,y1, legend_label=y1_l, color = 'blue', line_width=2)
p3.line(x,y2, legend_label=y2_l, color = 'red', line_width=2)
p3.line(x,y3, legend_label=y3_l, color = 'green', line_width=2)
p3.line(x,y4, legend_label=y4_l, color = 'yellow', line_width=2)
p3.legend.click_policy="mute"
p3.add_tools(HoverTool())
p3.title.text_font_size = '25px'
p3.legend.location = "top_left"
# show(p3)

# Figure 4 - Analysis Flow
x=data.loc[:,f.U2['runtime']]/3600
y1 = data.loc[:,f.U2['r8_feed']]
y2 = data.loc[:,f.U2['r6_nh3']]
y1_l='Analysis Dilution FLow [Nl/hr]'
y2_l='Analysis Raw flow [Nml/hr]'
p4 = figure(title = "Analysis Flow", x_axis_label = 'Time - hrs', y_axis_label='Flow - Nl/hr or NmL/hr')
p4.line(x,y1, legend_label=y1_l, color = 'blue', line_width=2)
p4.line(x,y2, legend_label=y2_l, color = 'red', line_width=2)
p4.legend.click_policy="mute"
p4.add_tools(HoverTool())
p4.title.text_font_size = '25px'
p4.legend.location = "top_left"
# show(p4)

save(column(children=[p1,p2,p3,p4], sizing_mode='stretch_width',height=500))

# Move the plotted filed
#path_folder = path[0:-21]
original =os.getcwd() + "\\" + plot_filename
target = path_folder + "\\" + plot_filename
shutil.move(original, target)

path_html = path_folder + "\\" + plot_filename
webbrowser.open(path_html)

print("All DONE - SUCCESS written\n")