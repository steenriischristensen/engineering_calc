REM This bat file will install the required packege and run the program

REM First install the requirede packages

REM TK For GUI creation
pip install tk==0.1.0 -q

REM inquirer for list creating
pip install inquirer==2.9.1 -q

REM install Pandas
pip install pandas==1.4.2 -q

REM for excel opening with pandas
pip install xlwt== 1.3.0 -q
pip install openpyxl==3.0.9 -q

REM Based on the packages used in the StandAlone function
pip install numpy==1.20.3 -q
pip install scipy==1.7.1 -q
pip install npTDMS==1.4.0 -q
pip install bokeh==2.4.1 -q
pip install PySimpleGUI==4.60.1 -q