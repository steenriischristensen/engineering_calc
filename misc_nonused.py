# -*- coding: utf-8 -*-
"""
Created on Thu Apr  7 14:52:25 2022

@author: u053554
"""


# write to excel file
# xl_writer = pd.ExcelWriter(path_file, if_sheet_exists='replace', engine='openpyxl', mode='a')
# book = openpyxl.load_workbook(path_file)
# xl_writer.book = book
# xl_writer.sheets = dict((ws.title, ws) for ws in book.worksheets)
# Data.to_excel(xl_writer, merge_cells=False, header=True, sheet_name='Rekorder1')
# xl_writer.save()
# xl_writer.close()
# #-----------
# options = {}
# options['strings_to_formulas'] = False
# options['strings_to_urls'] = False
# writer = pd.ExcelWriter(path_file,engine='xlsxwriter',options=options)
# Data.to_excel(writer, 'Rekorder1', index=False)
# writer.save()
# #-----------

# with pd.ExcelWriter(path_file,
#     mode="a",
#     engine="openpyxl",
#     if_sheet_exists="overlay",
#     # engine_kwargs={"keep_vba": True}
# ) as writer:
#     Data.to_excel(writer, sheet_name="Rekorder1")

#----------- Virker ikke - overskriver alle sheets og append virker ikke med xlsxwriter
# with pd.ExcelWriter(
#     path_file,
#     engine="xlsxwriter",
#     mode = "a",
#     engine_kwargs={"options": {"nan_inf_to_errors": True}}
# ) as writer:
#     Data.to_excel(writer, sheet_name="Rekorder1") 
# #-----------
# with pd.ExcelWriter(
#     path_file,
#     engine="openpyxl",
#     mode="a",
#     engine_kwargs={"keep_vba": True}
# ) as writer:
#     Data.to_excel(writer, sheet_name="Rekorder1")  

#-----
# book = load_workbook(path_file)
# #writer = pd.ExcelWriter(path_file, engine='openpyxl') 
# writer = pd.ExcelWriter(path_file, engine='openpyxl', mode = 'a', if_sheet_exists='replace') 
# writer.book = book

# ## ExcelWriter for some reason uses writer.sheets to access the sheet.
# ## If you leave it empty it will not know that sheet Main is already there
# ## and will create a new sheet.

# writer.sheets = dict((ws.title, ws) for ws in book.worksheets)

# Data.to_excel(writer, "Rekorder1")

# writer.save()

#-----

# ExcelWorkbook = load_workbook(path_file)
# writer = pd.ExcelWriter(path_file, mode="a", engine = 'openpyxl', if_sheet_exists = 'replace')
# writer.book = ExcelWorkbook
# Data.to_excel(writer, sheet_name = "Rekorder1")
# writer.save()
# writer.close()

#-----


# with pd.ExcelWriter(path_file,
#     mode="a",
#     engine="openpyxl",
#     if_sheet_exists="replace",
# ) as writer:
#     Data.to_excel(writer, sheet_name="Rekorder1")
    
 
    
#    ------
# import xlwings as xw

# #load workbook that has a chart in it
# wb = xw.Book(path_file)

# ws = wb.sheets['Rekorder1']

# ws.range('A1').options(index=False).value = Data

# xw.apps[0].quit()