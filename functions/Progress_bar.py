# -*- coding: utf-8 -*-
"""
Created on Thu Jul 28 21:08:03 2022

@author: u053554
"""

import PySimpleGUI as sg

# Create the Progress Bar Window
# layout the window
playout = [[sg.Text('Running - pls. wait')],
           [sg.ProgressBar(100, orientation='h',
                           size=(20, 20), key='progressbar')],
           [sg.Cancel()]]
# create the window`
pwindow = sg.Window('Progress', playout)
progress_bar = pwindow['progressbar']
event, values = pwindow.read(timeout=10)
if event == 'Cancel' or event == sg.WIN_CLOSED:
    pwindow.close()
