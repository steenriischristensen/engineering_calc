# -*- coding: utf-8 -*-
"""
Created on Sun May 29 21:16:33 2022

@author: u053554
"""

# Load the data based on User choosen TDMS file
data=f.read_tdms(path)
# data = data.iloc[:,0:104]
# Instead of the old method where a specific number of columns where used, now the full is loaded until DSC starts which is the non data part
data_test = data.columns.str.find("DSC")
data_end=(data_test.values != -1).argmax()
data = data.iloc[:,0:data_end]
data.columns=data.columns.str.replace("/'Rekorder1'/","")
data.columns = data.columns.str.replace("'","")
print("\nTDMS Data loaded \n")

# Reading xml data
#xml_data = pd.read_xml(path_xml) 
xml_tree = et.parse(path_xml)
xml_root = xml_tree.getroot()
print("xml data loaded \n")

# Run Number 
xml_run_number = xml_root.findall(path='./test/test_metadata/internal_experiment_id')[0].text.strip()

# U-Number 
xml_unumber = xml_root.findall(path='./test/business_metadata/external_experiment_id')[0].text.strip()

# cat type 
xml_cat = xml_root.findall(path='./test/test_metadata/samples/sample/*/lot')[0].text.strip()

# Mass 
xml_mass = xml_root.findall(path='./test/test_metadata/samples/sample/*/dimension')[0].text.strip()

# Comment 
xml_comment = xml_root.findall(path='./test/test_metadata/samples/sample/*/state')[0].text.strip()

# Creating a datafram containing the xml data:
xmldataf = {'what': ['Run Number', 'U-number', 'Catalyst Type', 'Mass Loaded (g)','Comment'],
        'Values': [xml_run_number, xml_unumber, xml_cat, xml_mass, xml_comment ]}  
  
# Create DataFrame  
xml_df = pd.DataFrame(xmldataf)  
print("xml data saved \n")

print("Copying basic excel sheet\n")

# Get current location
overall_path = os.getcwd()

# Change to correct backslash
overall_path = overall_path.replace(os.sep, '/')



    

# Direction of the file
overall_path_U3_Filter = overall_path+copy_file_location

# Destination folder without tdms filename
path_folder = path[0:-21]
path_file = path_folder+"/R"+path_folder[-6:]+".xlsx"

f.copy_file(overall_path_U3_Filter, path_file)
print("Done copying\n")

# Writing data to copyied data file
print("Writing data to raw file\n")

# For testing the datafile can be shortenen
# data=data.iloc[0:1000,0:10]