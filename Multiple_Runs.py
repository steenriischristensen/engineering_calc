# -*- coding: utf-8 -*-
"""
Created on Mon Mar 21 21:27:35 2022

@author: u053554, Steen Riis Christensen
"""
import numpy as np
import scipy as sp
from scipy import stats
from nptdms import TdmsFile
from nptdms import tdms
import os
from tkinter import filedialog
import func as f
import inquirer as iq
import pandas as pd
import openpyxl
from openpyxl import load_workbook
import xml.etree.ElementTree as et
import shutil
import webbrowser
from bokeh.plotting import figure, show, output_file, save
from bokeh.models.tools import HoverTool
from bokeh.layouts import row, column
from bokeh.models import LinearAxis, Range1d
import subprocess
import sys
import PySimpleGUI as sg
# This script is made for data analysis on test units, but made for running...
# in the comand promt


print(test_units.get())
print(cat_types.get()) 

# Promt the User to get the file location
path = filedialog.askopenfilename()

# Assume that an xml file also exist
path_xml = path.replace(".tdms", ".xml")

# units = ["Unit 1","Unit 2","Unit 3","Monolith Pilot"]

# types = [["Monolith"],
#           ["Monolith","Plader"],
#           ["Monolith","Filter"],
#           ["Monolith"]]

# Test which unit was chosen by the user

if test_units.get() == units[1]: # Unit 2 - R5R8
    if cat_types.get() == types[1][0]: # Std. DNX
        copy_file_location = "/Copy/U2_std_DeNox.xlsx"
        exec(open("MR_U2.py").read())

if test_units.get() == units[2]: # Unit 3
    if cat_types.get() == types[2][0]: # Monolith
        copy_file_location = "/Copy/U3_Monolith.xlsx"
        exec(open("MR_U3.py").read())
    
    elif cat_types.get() == types[2][1]: # Filter
        copy_file_location = "/Copy/U3_SCPF.xlsx"
        exec(open("MR_U3.py").read())


with pd.ExcelWriter(
        path_file,
        mode="a",
        engine="openpyxl",
        if_sheet_exists="replace",
) as writer:
    data.to_excel(writer, sheet_name="Rekorder1")
    
print("Data written \n")

print("Writting xml data \n")
    
with pd.ExcelWriter(
        path_file,
        mode="a",
        engine="openpyxl",
        if_sheet_exists="replace",
) as writer:
    xml_df.to_excel(writer, sheet_name="Run_info")    

print("xml written\n")

if test_units.get() == units[1]: # Unit 2
    exec(open("MR_U2_plot.py").read())

if test_units.get() == units[2]: # Unit 3
    exec(open("MR_U3_plot.py").read())

print("All DONE - SUCCESS written\n")
