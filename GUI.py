# -*- coding: utf-8 -*-
"""
Creator: Steen Riis Christensen
Date: 01-01-2022
What is it: This is the main GUI for the Engineering Calc program
"""

import tkinter as tk
from tkinter import ttk
import func as f

"""                     Introduction CODE   """
gui = tk.Tk()
gui.title("Engineering Calculator")
gui.geometry("500x500")
greeting = tk.Label(text="Welcome to the SECC R&D Program", 
                    font= (None,15))
                    
s = ttk.Separator(gui, orient='horizontal')

"""                         UNIT 3 CODE       """

lf3 = ttk.Labelframe(gui, text="Unit 3 Programs")

btn_u3_filter = ttk.Button(
    master= lf3,
    text = "Calculate Performance",
    command = lambda: [f.read_tdms()])


choices_u3 = ["Filter", "Monolith", "Raw"]
choicesvar_u3 = tk.StringVar(value=choices_u3)
l_u3 = tk.Listbox(lf3, listvariable = choicesvar_u3)


greeting.pack()
s.pack(fill='x')
lf3.pack(fill='x')
l_u3.grid(row = 0, column = 0)
btn_u3_filter.grid(row=0 , column=40)
gui.mainloop()
