# -*- coding: utf-8 -*-
"""
Created on Tue May 24 21:27:14 2022

@author: u053554
"""
import numpy as np
import scipy as sp
from scipy import stats
from nptdms import TdmsFile
from nptdms import tdms
import os
from tkinter import filedialog
import func as f
import xml.etree.ElementTree as et
import pandas as pd
import shutil

from bokeh.plotting import figure, show, output_file, save
from bokeh.models.tools import HoverTool
from bokeh.layouts import row, column
from bokeh.models import LinearAxis, Range1d

path='C:/Users/u053554/OneDrive - Umicore/Synced/Documents/Python Scripts/For_Program_test/PST_SECC_000222/PST_SECC_000222.tdms'
data=f.read_tdms(path)
data = data.iloc[:,0:104]
data.columns=data.columns.str.replace("/'Rekorder1'/","")
data.columns = data.columns.str.replace("'","")

plot_filename = 'Experimental_Plots.html'
output_file(filename=plot_filename, title="Plots from experiment")
# Figure 1 - Temperature In oven zones
x=data.loc[:,f.U3['runtime']]/3600
y1 = data.loc[:,f.U3['oven_temp_1']]
y2 = data.loc[:,f.U3['oven_temp_2']]
y3 = data.loc[:,f.U3['oven_temp_3']]
y1_l='TIC_331_PV'
y2_l='TIC_332_PV'
y3_l='TIC_333_PV'
p1 = figure(title = "Temperatures in Oven Zones", x_axis_label = 'Time - hrs', y_axis_label='Temperature - C')
p1.line(x,y1, legend_label=y1_l, color = 'blue', line_width=2)
p1.line(x,y2, legend_label=y2_l, color = 'red', line_width=2)
p1.line(x,y3, legend_label=y3_l, color = 'green', line_width=2)
p1.legend.click_policy="mute"
p1.add_tools(HoverTool())
p1.title.text_font_size = '25px'
p1.legend.location = "top_left"
#show(p1)

# Figure 2 - Temperatures in Filter 
x=data.loc[:,f.U3['runtime']]/3600
y1 = data.loc[:,f.U3['reactor_temp_1']]
y2 = data.loc[:,f.U3['reactor_temp_2']]
y3 = data.loc[:,f.U3['reactor_temp_3']]
y4 = data.loc[:,f.U3['reactor_temp_4']]
y5 = data.loc[:,f.U3['reactor_temp_5']]
y1_l='TI_0304'
y2_l='TI_0305'
y3_l='TI_0306'
y4_l='TI_0307'
y5_l='TI_0308'
p2 = figure(title = "Temperatures inside filter", x_axis_label = 'Time - hrs', y_axis_label='Temperature - C')
p2.line(x,y1, legend_label=y1_l, color = 'blue', line_width=2)
p2.line(x,y2, legend_label=y2_l, color = 'red', line_width=2)
p2.line(x,y3, legend_label=y3_l, color = 'green', line_width=2)
p2.line(x,y4, legend_label=y4_l, color = 'purple', line_width=2)
p2.line(x,y5, legend_label=y5_l, color = 'black', line_width=2)
p2.legend.click_policy="mute"
p2.add_tools(HoverTool())
p2.title.text_font_size = '25px'
p2.legend.location = "top_left"
#show(p2)

# Figure 3 - FTIR Data 
x=data.loc[:,f.U3['runtime']]/3600
y1 = data.loc[:,f.U3['O2']]
y2 = data.loc[:,f.U3['H2O']]
y3 = data.loc[:,f.U3['NO']]
y4 = data.loc[:,f.U3['NO2']]
y5 = data.loc[:,f.U3['NH3']]
y1_l='O2'
y2_l='H2O'
y3_l='NO'
y4_l='NO2'
y5_l='NH3'
p3 = figure(title = "FTIR Measurements", x_axis_label = 'Time - hrs', y_axis_label='NO, NO2, or NH3 - ppm')
# Setting the second y axis range name and range
p3.extra_y_ranges = {"foo": Range1d(start=0, end=20)}
# Adding the second axis to the plot.  
p3.add_layout(LinearAxis(y_range_name="foo", axis_label='O2 or H2O - %'), 'right')
p3.line(x,y1, legend_label=y1_l, color = 'blue', line_width=2,y_range_name="foo")
p3.line(x,y2, legend_label=y2_l, color = 'yellow', line_width=2,y_range_name="foo")
p3.line(x,y3, legend_label=y3_l, color = 'green', line_width=2)
p3.line(x,y4, legend_label=y4_l, color = 'purple', line_width=2)
p3.line(x,y5, legend_label=y5_l, color = 'red', line_width=2)
p3.legend.click_policy="mute"
p3.add_tools(HoverTool())
p3.title.text_font_size = '25px'
p3.legend.location = "top_left"
#show(p3)
show(column(children=[p1,p2,p3], sizing_mode='stretch_width',height=500))

# Move the plotted filed
path_folder = path[0:-21]
original =os.getcwd() + "\\" + plot_filename
target = path_folder + "\\" + plot_filename
shutil.move(original, target)


U3={'timestamp' : 'DATUTC__________',
   'runtime' : 'TIMMEA__________',
   'flow' : 'VOFEXHP1D1500___',
   'inlet_temp_SP': 'TIC_0302_SP',
   'inlet_temp_pv': 'TZAL_0303',
   'DP': 'PDI_0320_mean',
   'oven_temp_1' : 'TIC_0331_PV',
   'oven_temp_2' : 'TIC_0332_PV',
   'oven_temp_3' : 'TIC_0333_PV',
   'reactor_temp_1' : 'TI_0304',
   'reactor_temp_2' : 'TI_0305',
   'reactor_temp_3' : 'TI_0306',
   'reactor_temp_4' : 'TI_0307',
   'reactor_temp_5' : 'TI_0308',
   'O2'   : 'CVOO2_P1F1600___',   
   'H2O' : 'CVOH2OP1F1600___',
   'N2O' : 'CVON2OP1F1600___',
   'NO' : 'CVONO_P1F1600___',
   'NO2' : 'CVONO2P1F1600___',
   'NH3' : 'CVONH3P1F1600___',
   'SO2' : 'CVOSO2P1F1600___',
   'CO2' : 'CVOCO2P1F1600___,',
   'CO' : 'CVOCO_P1F1600___',}  