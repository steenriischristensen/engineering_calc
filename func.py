# -*- coding: utf-8 -*-
"""
Created on Sun Jan  2 21:39:34 2022

@author: Steen Riis Christensen
This Script contians the different functions to be used.
"""

import numpy as np
import scipy as sp
from scipy import stats
from nptdms import TdmsFile
from nptdms import tdms
import os
from tkinter import filedialog
import shutil
from openpyxl import load_workbook
import pandas as pd

def reynolds(rho, v, Dh, my):
    """
    Calculates the Reynolds number

    Parameters
    ----------
    rho : Density, [kg/m3], Scalar.
    v : Velocity, [m/s] Scalar.
    Dh : Hydraulic diameter, [m], Scalar.
    my : Dynamic viscosity, [Pa*s], Scalar.

    Returns
    -------
    Reynolds number, scalar. Re < 2300 => Laminar flow
                     Re > 2900 => Turbulent flow 
    Example of call: rey=f.reynolds(1, 2, 0.05, 1.3*10**-5)                 

    """
    reynolds = rho * v * Dh/my
    
    return reynolds

def read_tdms(path = "Empty"):
    """
    Read tdms files and report back the info as data frames
    
    Parameters
    ------------
    path : File path, optional argument

    Returns
    -------
    tdms_data : Data frames with the data points

    """
    
    if path == "Empty":
        path = filedialog.askopenfilename()
    
    tdms_file=TdmsFile.read(path)
    tdms_data = tdms_file.as_dataframe()
    print("TDMS file loaded")
    
    return tdms_data


def copy_file (original, target):
    """
    This function copies a file from one location to another based on path.
    
    Parameters
    ------------
    original : File path of original file to be copyied
    target :  Destination of original file to be copyied

    Returns
    -------
    
    """
    shutil.copy(original, target)
    
#A dictionary for easier access to SARA tags. Tags can now be accessed as e.g. T['H2O'] 
U3={'timestamp' : 'DATUTC__________',
   'runtime' : 'TIMMEA__________',
   'flow' : 'VOFEXHP1D1500___',
   'inlet_temp_SP': 'TIC_0302_SP',
   'inlet_temp_pv': 'TZAL_0303',
   'DP': 'PDI_0320_mean',
   'oven_temp_1' : 'TIC_0331_PV',
   'oven_temp_2' : 'TIC_0332_PV',
   'oven_temp_3' : 'TIC_0333_PV',
   'reactor_temp_1' : 'TI_0304',
   'reactor_temp_2' : 'TI_0305',
   'reactor_temp_3' : 'TI_0306',
   'reactor_temp_4' : 'TI_0307',
   'reactor_temp_5' : 'TI_0308',
   'O2'   : 'CVOO2_P1F1600___',   
   'H2O' : 'CVOH2OP1F1600___',
   'N2O' : 'CVON2OP1F1600___',
   'NO' : 'CVONO_P1F1600___',
   'NO2' : 'CVONO2P1F1600___',
   'NH3' : 'CVONH3P1F1600___',
   'SO2' : 'CVOSO2P1F1600___',
   'CO2' : 'CVOCO2P1F1600___',
   'CO' : 'CVOCO_P1F1600___',
   'C3H6' : 'CVOC36P1F1600___',
   'CH4' : 'CVOCH4P1F1600___',
   'CHO' : 'CVOCHOP1F1600___',
   'HCN' : 'CVOHCNP1F1600___',
   'C2H6' : 'CVOC26P1F1600___',
   'C3H8' : 'CVOC38P1F1600___',
   'C6H14' : 'CVOHEXP1F1600___',
   'C5H12' : 'CVOPENP1F1600___',
   'Ethanamine' : 'CVONHXP1F1600___',}       

#A dictionary for easier access to SARA tags. Tags can now be accessed as e.g. T['H2O'] 
U2={'timestamp' : 'DATUTC__________',
   'runtime' : 'TIMMEA__________',
   'mfc_n2' : 'FIC_05_PV',
   'mfc_no' : 'FIC_08_PV',
   'mfc_so2' : 'FIC_07_PV',
   'mfc_air' : 'FIC_06_PV',
   'r5_feed' : 'FIC_51_PV',
   'r5_nh3' : 'FIC_52_PV',
   'r5_h2o' : 'FIC_53_PV',
   'r6_feed' : 'FIC_61_PV',
   'r6_nh3' : 'FIC_62_PV',
   'r6_h2o' : 'FIC_63_PV',
   'r7_feed' : 'FIC_71_PV',
   'r7_nh3' : 'FIC_72_PV',
   'r7_h2o' : 'FIC_73_PV',
   'r8_feed' : 'FIC_81_PV',
   'r8_nh3' : 'FIC_82_PV',
   'r8_h2o' : 'FIC_83_PV',
   'cld_nox' : 'AI_2_NOx',
   'cld_dp' : 'PDI_02',
   'r5_temp' : 'TI_54_PV',
   'r6_temp' : 'TI_64_PV',
   'r7_temp' : 'TI_74_PV',
   'r8_temp' : 'TI_84_PV',   
   }
   
def words(string, sep):
    #divides a given string in an array of substrings, using the defined separation character. Consecutive
    #separation characters are interpreted as one separation, and will therefore never be a part of the output string.
    #Examples: words('This is a string',' ' )= ['This','is','a','string]
    #words('This is a string','s')=['Thi', ' i', ' a ', 'tring']: note that the blanks are a part of the output now.
    #words('This is a string,'a')=['This is ', ' string']
    string+=sep
    outlist=list([])
    curstring=''
    if string[0]!=sep:
        curstring+=string[0]
    for j in range(1,len(string)):
        if string[j]!= sep:
            curstring+=string[j]
        else:
            if string[j]!=string[j-1]:
                outlist.append(curstring)
                curstring=''
    
    return(outlist)
#
    
def statusbar(val, max, Title, pkey):
    """
    Creates a new Window with a progress bar that can be updated with the key.Update
    
    Parameters
    ------------
    val : Initial value for progress bar
    max : Value at 100% for progress bar
    Title : Text in progress bar
    pkey : The key used for the update procedure

    Returns
    -------
    Window : returns the window containing the progress bar

    """
    import PySimpleGUI as sg

    # layout the window
    layout = [[sg.Text(Title)],
              [sg.ProgressBar(max, orientation='h', size=(20, 20), key=pkey)],
              [sg.Cancel()]]

    # create the window`
    window = sg.Window('Status', layout)
    pkey = window[pkey]
    # loop that would normally do something useful
    event, values = window.read(timeout=10)
    if event == 'Cancel'  or event == sg.WIN_CLOSED:
        window.close()
    
    pkey.UpdateBar(val)
    return(window)

# def append_df_to_excel(filename, df, sheet_name='Sheet1', startrow=None,
#                        truncate_sheet=False, 
#                        **to_excel_kwargs):
#     """
#     Append a DataFrame [df] to existing Excel file [filename]
#     into [sheet_name] Sheet.
#     If [filename] doesn't exist, then this function will create it.
#     Parameters:
#       filename : File path or existing ExcelWriter
#                  (Example: '/path/to/file.xlsx')
#       df : dataframe to save to workbook
#       sheet_name : Name of sheet which will contain DataFrame.
#                    (default: 'Sheet1')
#       startrow : upper left cell row to dump data frame.
#                  Per default (startrow=None) calculate the last row
#                  in the existing DF and write to the next row...
#       truncate_sheet : truncate (remove and recreate) [sheet_name]
#                        before writing DataFrame to Excel file
#       to_excel_kwargs : arguments which will be passed to `DataFrame.to_excel()`
#                         [can be dictionary]
#     Returns: None
#     """

#     # ignore [engine] parameter if it was passed
#     if 'engine' in to_excel_kwargs:
#         to_excel_kwargs.pop('engine')

#     writer = pd.ExcelWriter(filename, engine='openpyxl')

#     # Python 2.x: define [FileNotFoundError] exception if it doesn't exist 
#     # try:
#     #     FileNotFoundError
#     # except NameError:
#     #     FileNotFoundError = IOError


#     try:
#         # try to open an existing workbook
#         writer.book = load_workbook(filename)

#         # get the last row in the existing Excel sheet
#         # if it was not specified explicitly
#         if startrow is None and sheet_name in writer.book.sheetnames:
#             startrow = writer.book[sheet_name].max_row

#         # truncate sheet
#         if truncate_sheet and sheet_name in writer.book.sheetnames:
#             # index of [sheet_name] sheet
#             idx = writer.book.sheetnames.index(sheet_name)
#             # remove [sheet_name]
#             writer.book.remove(writer.book.worksheets[idx])
#             # create an empty sheet [sheet_name] using old index
#             writer.book.create_sheet(sheet_name, idx)

#         # copy existing sheets
#         writer.sheets = {ws.title:ws for ws in writer.book.worksheets}
#     except FileNotFoundError:
#         # file does not exist yet, we will create it
#         pass

#     if startrow is None:
#         startrow = 0

#     # write out the new sheet
#     df.to_excel(writer, sheet_name, startrow=startrow, **to_excel_kwargs)

#     # save the workbook
#     writer.save()