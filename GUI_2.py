# -*- coding: utf-8 -*-
"""
Created on Sun May 29 23:01:30 2022

@author: u053554
"""
import PySimpleGUI as sg

# Defining the possible test units
Units = ['None', 'Unit 1', 'Unit 2', 'Unit 3', 'Monolith Pilot']

# Defining the possible catalyst test types
Catalyst_types =[["None"], # None
                 ["Monolith"], # For Unit 1
                 ["Monolith", "Plates"], # For Unit 2
                 ["Monolith", "Filter"], # For Unit 3
                 ["Monolith"]] # For Monolith Pilot

sg.theme('DarkAmber')   # Add a little color to your windows
# All the stuff inside your window. This is the PSG magic code compactor...
layout = [  [sg.Text('Welcome to the SECC Test Unit Data Program - V1.0')],
            [sg.Text('Pls. Choose the Unit and type of experiment')],
            [sg.Text('')],
            [sg.Text('')],
            [sg.Text('Choose the unit',size=(20, 1), font='Lucida',justification='left')],
            [sg.Combo(Units,default_value='None',key='unit_choice')],
            [sg.Text('Choose the Catalyst type',size=(20, 1), font='Lucida',justification='left')],
            [sg.Combo(Catalyst_types[Units.index(values['unit_choice'])],default_value='None',key='catalyst_choice')],
            [sg.OK(), sg.Cancel()]]

# Create the Window
window = sg.Window('SECC - Test Program', layout)
# Event Loop to process "events"
while True:             
    event, values = window.read()
    if event in (sg.WIN_CLOSED, 'Cancel'):
        break
    

    

    
    

window.close()