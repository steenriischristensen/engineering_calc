# -*- coding: utf-8 -*-
"""
Created on Sun May 29 23:28:11 2022

@author: u053554
"""

import tkinter
from tkinter import ttk
from tkinter import messagebox

def callback(eventObject):
    abc = eventObject.widget.get()
    unit = test_units.get()
    index=units.index(unit)
    cat_types.config(values=types[index])
    
def on_closing():
    if messagebox.askokcancel("Quit", "Do you want to quit?"):
        root.destroy()
        root.quit() 
        
def not_ready():
    if messagebox.askretrycancel(title= "Error/Warning", message="The program is not yet ready for that unit") == True:
        return
    else:
        on_closing()
        
def run_program():
    if test_units.get() == units[0]: # Unit 1    
        not_ready()
    elif test_units.get() == units[1]: # Unit 2
        exec(open("Multiple_Runs.py").read())
    elif test_units.get() == units[2]: # Unit 3
        exec(open("Multiple_Runs.py").read())
    elif test_units.get() == units[1]: # monolith Pilot
        not_ready()
    else:
        not_ready()
    

root = tkinter.Tk()
root.title("Engineering Calculator")
root.geometry("500x500")
greeting = tkinter.Label(root, text="Welcome to the SECC R&D Program\n", 
                    font= (None,15))
                    
s = ttk.Separator(root, orient='horizontal')

text1 = tkinter.Label(root, text="Please First choose the Unit used to measure the data\n", 
                    font= (None,12))
''' 
widgets are added here 
'''
units = ["Unit 1 (not ready)","Unit 2","Unit 3","Monolith Pilot (not ready)"]

types = [["Monolith"],
          ["Std. DNX"],
          ["Monolith","Filter"],
          ["Monolith"]]

test_units = ttk.Combobox(root, width=37, value=(units))
#car_brand.grid(row=3, column=1, columnspan=2, padx=10, pady=2, sticky='w')


text2 = tkinter.Label(root, text="Then choose the type of data file that should be created\n", 
                    font= (None,12))
cat_types = ttk.Combobox(root, width=37)
#car_model.grid(row=4, column=1, columnspan=2, padx=10, pady=2, sticky='w')
cat_types.bind('<Button-1>', callback)

#btn_f = ttk.Labelframe(root, text="")
btn = ttk.Button(
    root,
    text = "Calculate Performance",
    command = run_program)


root.protocol("WM_DELETE_WINDOW", on_closing)

greeting.pack()
s.pack(fill='x')
text1.pack()
test_units.pack()
text2.pack()
cat_types.pack()
#btn_f.pack()
btn.pack()
root.mainloop()