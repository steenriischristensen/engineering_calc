# -*- coding: utf-8 -*-
"""
Created on Thu Jun  9 00:32:43 2022

@author: u053554
"""

# Load the data based on User choosen TDMS file
data=f.read_tdms(path)
data = data.iloc[:,0:114]
data.columns=data.columns.str.replace("/'Rekorder1'/","")
data.columns = data.columns.str.replace("'","")
print("\nTDMS Data loaded \n")

# Reading xml data
#xml_data = pd.read_xml(path_xml) 
xml_tree = et.parse(path_xml)
xml_root = xml_tree.getroot()
print("xml data loaded \n")

# Run Number 
xml_run_number = xml_root.findall(path='./test/test_metadata/internal_experiment_id')[0].text.strip()

comment=xml_root.findall(path='./test/test_metadata/samples/sample/*/description')[0].text.strip()
for w in f.words(comment,'\n'):
   x=f.words(w,'#')
   for i in range(len(x)):
       if (len(x)>1):
           if ('R5 Run Number' in x[0]):
               r5_run_number = x[1].strip()
           if ('R5 U-numbe' in x[0]):
               r5_unumber = x[1].strip()
           if ('R5 Mass Loaded (g)' in x[0]):
               r5_cat_mass = x[1].strip()
               r5_cat_mass = r5_cat_mass.replace(",", ".")
           if ('R5 Dry Flow Meas (Nl/hr)' in x[0]):
               r5_flow = x[1].strip()
               r5_flow = r5_flow.replace(",", ".")               
           if ('R6 Run Number' in x[0]):
               r6_run_number = x[1].strip()
           if ('R6 U-numbe' in x[0]):
               r6_unumber = x[1].strip()
           if ('R6 Mass Loaded (g)' in x[0]):
               r6_cat_mass = x[1].strip()
               r6_cat_mass = r6_cat_mass.replace(",", ".")
           if ('R6 Dry Flow Meas (Nl/hr)' in x[0]):
               r6_flow = x[1].strip()
               r6_flow = r6_flow.replace(",", ".")                     
           if ('R7 Run Number' in x[0]):
               r7_run_number = x[1].strip()
           if ('R7 U-numbe' in x[0]):
               r7_unumber = x[1].strip()
           if ('R7 Mass Loaded (g)' in x[0]):
               r7_cat_mass = x[1].strip()
               r7_cat_mass = r7_cat_mass.replace(",", ".")
           if ('R7 Dry Flow Meas (Nl/hr)' in x[0]):
               r7_flow = x[1].strip()  
               r7_flow = r7_flow.replace(",", ".")                    
           if ('R8 Run Number' in x[0]):
               r8_run_number = x[1].strip()
           if ('R8 U-numbe' in x[0]):
               r8_unumber = x[1].strip()
           if ('R8 Mass Loaded (g)' in x[0]):
               r8_cat_mass = x[1].strip()
               r8_cat_mass = r8_cat_mass.replace(",", ".")
           if ('R8 Dry Flow Meas (Nl/hr)' in x[0]):
               r8_flow = x[1].strip()
               r8_flow = r8_flow.replace(",", ".")   

if len(comment)>1:
    # Creating a datafram containing the xml data:
    xmldataf = {'what': ['Run Number', 'R5 Run Number', 'R5 U-Number', 'R5 Mass Loaded (g)','R5 Dry flow', 'R6 Run Number', 'R6 U-Number', 'R6 Mass Loaded (g)','R6 Dry flow', 'R7 Run Number', 'R7 U-Number', 'R7 Mass Loaded (g)','R7 Dry flow', 'R8 Run Number', 'R8 U-Number', 'R8 Mass Loaded (g)','R8 Dry flow'],
        'Values': [xml_run_number, r5_run_number, r5_unumber, r5_cat_mass, r5_flow, r6_run_number, r6_unumber, r6_cat_mass, r6_flow, r7_run_number, r7_unumber, r7_cat_mass, r7_flow, r8_run_number, r8_unumber, r8_cat_mass, r8_flow ]}  
else:
    xmldataf = {'what': ['Info'],
                'Values': ['no info found']}
    
# Create DataFrame  
xml_df = pd.DataFrame(xmldataf)  
print("xml data saved \n")

print("Copying basic excel sheet\n")

# Get current location
overall_path = os.getcwd()

# Change to correct backslash
overall_path = overall_path.replace(os.sep, '/')

# Direction of the file
overall_path_U3_Filter = overall_path+copy_file_location

# Destination folder without tdms filename
path_folder = path[0:-22]
path_file = path_folder+"/R"+path_folder[-6:]+"_R5-x_R6-x_R7-x_R8-x.xlsx"

f.copy_file(overall_path_U3_Filter, path_file)
print("Done copying\n")

# Writing data to copyied data file
print("Writing data to raw file\n")